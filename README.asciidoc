Top Coin Correlator
===================
:author: Martin Keiblinger

This is a simple collection of tools and manuals to create a Bitcoin to Altcoin correlation table. 

## Installing

After making sure you've got https://www.python.org/[Python] and https://pypi.org/project/pipenv/[Pipenv] installed, you may just run: `pipenv install` in the folder.

Afterwards you're able to execute the programs as described.

## Creating Top Coin Correlations

This is basically a one-off thing which I run once and probably forget forever. But if someone else is interested or wants to check the data (don't trust, verify ;-), please go through the following steps:

### 1. Assembling Top List

https://coinmarketcap.com/[Coinmarketcap] is a lazy loading Javascript driven webpage. In short: A bit of a hassle to parse via python program. Therefore, to collect the top list of your coins, you need to do this:

1. Go to https://coinmarketcap.com/[Coinmarketcap]
2. Open the browser DEV console by either hitting `F12` or right-click and click on `Inspect`
3. Make sure to swith to the Console Tab

Now you're able to run this script to extract the Coin symbols:

.Extractor JS
----
let coins_list2 = [];
document.querySelectorAll('p.coin-item-symbol').forEach(item => { if (item.innerText) { coins_list2.push(item.innerText) } })
JSON.stringify(coins_list2)
----

### 2. Fetching Necessary Price Data

You copy the list of coin symbols without the quotation marks. This  should look similar to this

.Coin Symbols
----
["BTC","ETH","USDT","BNB","USDC","BUSD","XRP","ADA","DOGE","MATIC","DOT","DAI","SOL","SHIB","UNI","TRX","LTC","AVAX","WBTC","LEO"]
----

Insert this into the `fetch_price_data.py` script as value for the `coin_symbols` variable. Now run the script via `pipenv` like so:

.Running Fetcher
----
pipenv run python3 ./fetch_price_data.py
----

### 3. Calculating Correlations

Python does the heavy lifting here with the help of Pandas. For you, you'll need to copy again the coin symbols to the `coin_symbols` variable in the `create_correlations.py` file and then execute it like so:

.Running Fetcher
----
pipenv run python3 ./create_correlations.py
----

Afterwards, you'll receive a file named `correlations.csv` containing all the correlations you'll added.

Should look similar to this:

.Example correlations.csv (Created 15th of November, 2022)
----
Position,Symbol,Correlation with Bitcoin
1,BTC,1.0
2,ETH,0.8970965183325709
3,USDT,0.16595250997068622
4,BNB,0.8468516215250046
5,USDC,0.06289122689219961
6,BUSD,-0.006961182230262237
7,XRP,0.7713212488583229
8,ADA,0.7903646308490607
9,DOGE,0.717002051262791
10,MATIC,0.7863276523668495
11,DOT,0.8048099595552635
12,DAI,0.20978116098965396
13,SOL,0.7703011581037306
14,SHIB,0.6408248025482907
15,TRX,0.6691595043404077
16,UNI,0.010545609402758544
17,LTC,0.8360791068989575
18,AVAX,0.7536819331094897
19,WBTC,0.9969668282867367
20,LEO,0.22193117372191207
21,LINK,0.7778798937341387
22,ATOM,0.7061197193332156
23,ETC,0.7522664295624965
24,XMR,0.7405952947380761
25,XLM,0.8189849118351554
26,BCH,0.8407769931469237
27,TON,0.7245814754509257
28,ALGO,0.7473396685797079
29,CRO,0.7761963631773066
30,NEAR,0.6877845279827907
31,QNT,0.6363865821538349
32,VET,0.8139861786556408
33,FIL,0.6952077679305625
34,FLOW,0.7809925265380722
35,CHZ,0.6961224122146699
36,OKB,0.6613990980931149
37,HBAR,0.7694685777212562
38,ICP,0.6884126921808433
39,XCN,0.9987074722367768
40,EGLD,0.647366004094392
41,LUNC,-0.007736019899675109
42,EOS,0.7725776784894502
43,TWT,0.5809080759742637
44,USDP,0.132578326582133
45,XTZ,0.757401008257437
46,APE,0.1314783074724437
47,THETA,0.7254251835129889
48,SAND,0.685843023903921
49,TUSD,0.052272011552353025
50,AAVE,0.7862578980000378
----
