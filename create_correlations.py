import csv
import pandas

coin_symbols = ["BTC","ETH","USDT","BNB","USDC","BUSD","XRP","ADA","DOGE","MATIC","DOT","DAI","SOL","SHIB","TRX","UNI","LTC","AVAX","WBTC","LEO","LINK","ATOM","ETC","XMR","XLM","BCH","TON","ALGO","CRO","NEAR","QNT","VET","FIL","FLOW","CHZ","OKB","HBAR","ICP","XCN","EGLD","LUNC","EOS","TWT","USDP","XTZ","APE","THETA","SAND","TUSD","AAVE"]
coin_dataframes = {}

print("Loading CSV")

for coin_symbol in coin_symbols:
    coin_df = pandas.read_csv("./{0}.csv".format(coin_symbol))
    # Quotient
    # coin_df['Relative'] = coin_df['Open'] / coin_df['Close']
    # Relative Change
    # (FV − IV) ÷ IV × 100
    coin_df['Relative'] = (coin_df['Close'] - coin_df['Open']) / coin_df['Open'] * 100
    coin_dataframes[coin_symbol] = coin_df

print("Generating correlations")

with open('correlations.csv', 'w') as correlations_fp:
    csvwriter = csv.writer(correlations_fp)
    csvwriter.writerow([ 'Position', 'Symbol', 'Correlation with Bitcoin' ])

    pos = 1

    for coin_symbol in coin_symbols:
        correlation_val = coin_dataframes[coin_symbol]['Relative'].corr(coin_dataframes['BTC']['Relative'])
        csvwriter.writerow([ pos, coin_symbol, correlation_val ])
        pos += 1

print('Written file correlations.csv')
