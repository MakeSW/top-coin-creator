import os.path
import requests
import pyuser_agent

coin_symbols = ["BTC","ETH","USDT","BNB","USDC","BUSD","XRP","ADA","DOGE","MATIC","DOT","DAI","SOL","SHIB","TRX","UNI","LTC","AVAX","WBTC","LEO","LINK","ATOM","ETC","XMR","XLM","BCH","TON","ALGO","CRO","NEAR","QNT","VET","FIL","FLOW","CHZ","OKB","HBAR","ICP","XCN","EGLD","LUNC","EOS","TWT","USDP","XTZ","APE","THETA","SAND","TUSD","AAVE"]
link_template = "https://query1.finance.yahoo.com/v7/finance/download/{0}-USD?period1=1636964573&period2=1668500573&interval=1d&events=history&includeAdjustedClose=true"
ua = pyuser_agent.UA()

for coin_symbol in coin_symbols:
    file_name = "{0}.csv".format(coin_symbol)

    if os.path.exists("./{0}".format(file_name)):
        print("Omitting fetch of {0} as file {1} already exists".format(coin_symbol, file_name))
    else:
        # they don't want us to extract the CSVs automatically - so we put on a mask and act as a
        # random browser -> don't overdo that or they'll close this and stopping the party for all
        # of us!
        headers = {
            'User-Agent': ua.random
        }

        

        req = requests.request('GET', link_template.format(coin_symbol), headers=headers)
        
        print("Trying URL:")
        print(link_template.format(coin_symbol))

        with open(file_name, 'wb') as f:
            f.write(req.content)
            print("Written {0}".format(file_name))
